﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Data.Entity;
using projetCatalogueProduit.Models;

namespace projetCatalogueProduit.Controllers
{
    public class CategorieController : Controller
    {
        CATALOGUE_Entities db = new CATALOGUE_Entities();
        // GET: Categorie
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult ModifierCategorie(int id)
        {

            try
            {
                ViewBag.listcategorie = db.CAT_CATEGORIE.ToList();
                CAT_CATEGORIE categorie = db.CAT_CATEGORIE.Find(id);
                if(categorie != null)
                {
                    return View("AjoutCategorie", categorie);
                }
                return RedirectToAction("AjoutCategorie");
            }
            catch (Exception e)
            {
                return HttpNotFound();
            }
        }

        [HttpPost]
        public ActionResult ModifierCategorie(CAT_CATEGORIE categorie)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    db.Entry(categorie).State = EntityState.Modified;  //Modification de notre categorie
                    categorie.DATA_SAISIE = DateTime.Now;
                    db.SaveChanges();
                }
                return RedirectToAction("AjoutCategorie");
            }
            catch (Exception e)
            {
                return HttpNotFound();
            }
        }

        public ActionResult SupprimerCategorie(int id)
        {
            try
            {
                CAT_CATEGORIE categorie = db.CAT_CATEGORIE.Find(id); // Rechercher la categorie
                if(categorie != null)
                {
                    db.CAT_CATEGORIE.Remove(categorie);  //Supprimer la categorie
                    db.SaveChanges(); // On enregistre le resultat
                }
                return RedirectToAction("AjoutCategorie");
            }
            catch (Exception e)
            {
                return HttpNotFound();
            }
        }

        public ActionResult AjoutCategorie()
        {
            try
            {
                ViewBag.listcategorie = db.CAT_CATEGORIE.ToList();
                return View();                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                            
            }
            catch (Exception e)
            {
                return HttpNotFound();
            }
        }

        [HttpPost]
        public ActionResult AjoutCategorie(CAT_CATEGORIE categorie)
        {

            try
            {
                if (ModelState.IsValid)
                {
                    categorie.DATA_SAISIE = DateTime.Now;
                    var  cate =categorie.LIBELLE_CAT.ToString().ToUpper();
                    categorie.LIBELLE_CAT = cate;
                    db.CAT_CATEGORIE.Add(categorie);
                    db.SaveChanges();
                }
                return RedirectToAction("ajoutcategorie");
            }
            catch (Exception e)
            {
                return HttpNotFound();
            }
        }
    }
}
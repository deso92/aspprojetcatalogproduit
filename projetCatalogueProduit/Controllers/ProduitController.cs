﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Data.Entity;
using projetCatalogueProduit.Models;
using System.IO;

namespace projetCatalogueProduit.Controllers
{
    public class ProduitController : Controller
    {
        CATALOGUE_Entities db = new CATALOGUE_Entities();
        // GET: Produit
        public ActionResult Index()
        {
            return View();
        }


        public ActionResult ModifierProduit(int id)
        {

            try
            {
                ViewBag.listecategorie = db.CAT_CATEGORIE.ToList();
                ViewBag.listeproduit = db.CAT_PRODUI.ToList();

                CAT_PRODUI produit = db.CAT_PRODUI.Find(id);
                if (produit != null)
                {
                    return View("AjoutProduit", produit);
                }
                return RedirectToAction("AjoutProduit");
            }
            catch (Exception e)
            {
                return HttpNotFound();
            }
        }

        [HttpPost]
        public ActionResult ModifierProduit(CAT_PRODUI produit)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    db.Entry(produit).State = EntityState.Modified;  //Modification de notre produit
                    if (Request.Files.Count > 0)
                    {
                        var file = Request.Files[0]; //Le nom de notre fichier
                        if (file != null && file.ContentLength > 0) //Si notre fichier est different de null et que sa taille est > 0 octet
                        {
                            var fileName = Path.GetFileName(file.FileName); //Recuperer le nom du fichier
                            var path = Path.Combine(Server.MapPath("~/Fichier"), fileName);  // Recuperer le chemin d'acces ou sera mis notre fichier
                            file.SaveAs(path);  //Enregistrer le tout sur le serveur

                            produit.IMAGE_PROD = fileName;
                            produit.URL_IMAGE_PROD = "/Fichier";

                        }
                    }
                    produit.DATE_SAISIE = DateTime.Now;
                    db.SaveChanges();
                }
                return RedirectToAction("AjoutProduit");
            }
            catch (Exception e)
            {
                return HttpNotFound();
            }
        }

        public ActionResult SupprimerProduit(int id)
        {
            try
            {
                CAT_PRODUI produit = db.CAT_PRODUI.Find(id); // Rechercher la categorie
                if (produit != null)
                {
                    db.CAT_PRODUI.Remove(produit);  //Supprimer la categorie
                    db.SaveChanges(); // On enregistre le resultat
                }
                return RedirectToAction("AjoutProduit");
            }
            catch (Exception e)
            {
                return HttpNotFound();
            }
        }

        public ActionResult AjoutProduit()
        {
            try
            {
                ViewBag.listeproduit = db.CAT_PRODUI.ToList();
                ViewBag.listecategorie = db.CAT_CATEGORIE.ToList();
                return View();
            }
            catch (Exception e)
            {
                return HttpNotFound();
            }
        }
        [HttpPost]
        public ActionResult AjoutProduit(CAT_PRODUI produit)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    if (Request.Files.Count > 0)
                    {
                        var file = Request.Files[0]; //Le nom de notre fichier
                        if(file != null && file.ContentLength > 0) //Si notre fichier est different de null et que sa taille est > 0 octet
                        {
                            var fileName = Path.GetFileName(file.FileName); //Recuperer le nom du fichier
                            var path = Path.Combine(Server.MapPath("~/Fichier"), fileName);  // Recuperer le chemin d'acces ou sera mis notre fichier
                            file.SaveAs(path);  //Enregistrer le tout sur le serveur

                            produit.IMAGE_PROD = fileName;
                            produit.URL_IMAGE_PROD = "/Fichier";

                        }
                    }
                    produit.DATE_SAISIE = DateTime.Now;
                    db.CAT_PRODUI.Add(produit);
                    db.SaveChanges();
                }
                return RedirectToAction("AjoutProduit");
            }
            catch (Exception e)
            {
                return HttpNotFound();
            }
        }
    }
}
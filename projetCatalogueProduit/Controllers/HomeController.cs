﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Data;
using projetCatalogueProduit.Models;

namespace projetCatalogueProduit.Controllers
{
    public class HomeController : Controller
    {
        // GET: Home
        public ActionResult Index()
        {
            CATALOGUE_Entities db = new CATALOGUE_Entities();
            ViewBag.listecategorie = db.CAT_CATEGORIE.ToList().OrderBy(r => r.LIBELLE_CAT); // Liste des categories rangees par ordre alphabetique
            return View();
        }
    }
}